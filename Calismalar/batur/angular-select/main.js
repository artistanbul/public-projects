var testApp = angular.module('testApp', []);
testApp.factory('Countries', function() {

    var Countries = {};
    Countries = [
        'Turkey',
        'Iraq',
        'Syria'
    ];
    return Countries;
});

testApp.factory('Cities', function() {

    var Cities = {};
    Cities = [
        ['Istanbul', 'Ankara', 'Izmir'],
        ['Baghdad', 'Karbala', 'Mosul'],
        ['Aleppo', 'Damascus', 'Latakia']
    ]
    return Cities;
});

function FormCtrl($scope, Countries, Cities) {
    $scope.countries = Countries;
//    $scope.cities = Cities;
    $scope.getOptions = function() {
        // just some silly stuff to get the key of what was selected since we are using simple arrays.
        var key = $scope.countries.indexOf($scope.country);
        // Here you could actually go out and fetch the options for a server.
        var myNewOptions = Cities[key];
        console.log(myNewOptions);
        // Now set the options.
        // If you got the results from a server, this would go in the callback
        $scope.cities = myNewOptions;
    };
}