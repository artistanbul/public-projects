<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
</head>
<body>
<div class="container" ng-app="testApp">
    <div class="row">
        <div class="span12">
            <form id="select-form" ng-controller="FormCtrl">
                <select name="country" class="select-country" ng-model="country" ng-options="country for country in countries" ng-change="getOptions()"></select>
                <select name="city" class="select-city" ng-model="city" ng-options="city for city in cities" ng-show="cities.length"></select>
            </form>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.1.5/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="main.js"></script>
</body>
</html>
