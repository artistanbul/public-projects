/*
Bütün kontrollerin boş bırakılma durumlarının kontrollerini yaptım abi o eksik kalmış.
*/
$(document).ready(function(){

/*Abi aşağıdaki kodda üzerine geldiğimiz anda boşalmasını istemiştin onu yaptırdım.
bütün input taglerini kullandım.
 if-else yapısını kullanarak boş olup olmadığına baktım abi daha sonra. */
  $("input").focus(function(){
	if ($(this).val()=="") {
		$("#Uyari").removeClass();
    	$("#Uyari").empty();
	}else{
	$("#Uyari").removeClass();
    	$("#Uyari").empty();
        $(this).val(null);
	}
  })

  $(".isim").blur(function(){
  	if ($(this).val()=="") {
  		$("#Uyari").removeClass();
    	$("#Uyari").empty();
    	$("#Uyari").append("Lütfen boş bırakmayınız.!");
    	$("#Uyari").addClass("error");
  	} 
  	else{
		if (isimKont($('.isim').val())) {
    		$("#Uyari").removeClass();
    		$("#Uyari").empty();
    		$("#Uyari").append("Başarılı.");
            $("#Uyari").addClass("success");

        }
    	else{
    		$("#Uyari").removeClass();
    		$("#Uyari").empty();
            $("#Uyari").append("Sayı değeri girildi. Lütfen metin giriniz.");
            $("#Uyari").addClass("error");
    		    }
  	} 
  });
  $(".soyisim").blur(function(){
		if ($(this).val()=="") {
			$("#Uyari_soy").removeClass();
            $("#Uyari_soy").empty();
            $("#Uyari_soy").append("Lütfen boş bırakmayınız.!");
            $("#Uyari_soy").addClass("error");
		} 
		else{
			if (soyisimKont($('.soyisim').val())) {
            	$("#Uyari_soy").removeClass();
            	$("#Uyari_soy").empty();
            	$("#Uyari_soy").append("Başarılı.");
            	$("#Uyari_soy").addClass("success");
        	}
    		else{
            	$("#Uyari_soy").removeClass();
            	$("#Uyari_soy").empty();
            	$("#Uyari_soy").append("Sayı değeri girildi. Lütfen metin giriniz.");
            	$("#Uyari_soy").addClass("error");
          
    }
		}

    
  });
   $(".email").blur(function(){
   	if ($(this).val()=="") {
		$("#Uyari_email").removeClass();
        $("#Uyari_email").empty();
        $("#Uyari_email").append("Lütfen boş bırakmayınız.!");
        $("#Uyari_email").addClass("error");
   	} 
   	else{
		if (mailKont($('.email').val())) {
        	$("#Uyari_email").removeClass();
        	$("#Uyari_email").empty();
        	$("#Uyari_email").append("Başarılı.");
       		$("#Uyari_email").addClass("success");
      
        }
    	else{
            $("#Uyari_email").removeClass();
            $("#Uyari_email").empty();
            $("#Uyari_email").append("Hatalı.");
            $("#Uyari_email").addClass("error");
    }
   	}
    
  });

  $(".url").blur(function(){
  	if ($(this).val()=="") {
		$("#Uyari_url").removeClass();
        $("#Uyari_url").empty();
        $("#Uyari_url").append("Lütfen boş bırakmayınız.!");
        $("#Uyari_url").addClass("error");

  	} 
  	else{
 		if (urlKont($('.url').val())) {
            $("#Uyari_url").removeClass();
            $("#Uyari_url").empty();
            $("#Uyari_url").append("Başarılı.");
            $("#Uyari_url").addClass("success");
      
        }
    	else{
            $("#Uyari_url").removeClass();
            $("#Uyari_url").empty();
            $("#Uyari_url").append("Hatalı.");
            $("#Uyari_url").addClass("error");
    }
  	};
   
  });
  

  /*
	asıl mesele burada abi. İnternette çok araştırdım. Phone mask with jquery diye aramalarımı yaptım 
	ama tek yol olarak html kısmına eklediğim js dosyası dosyası kullanılıyor.

	Fakat bu js dosyasında 9 kullanılarak maskeleme yapmış. Bende ilk başta çalışmadı.
	Çünkü sen +90 ile başlamasını istemiştin abi.
	o yüzden kodda da bi değişiklik yaptım.

	Normalde aşağıdaki N yerine 9 yazıyoruz. Ama düzenlemeden sonra N olarak kullanıyoruz. 
  */
  $.mask.definitions['@'] = '[0543]';
  $(".tel").mask("+90 (5@N) NNN NN NN"); 



  $(".tel").blur(function(){
  	if ($(this).val()=="") {
		$("#Uyari_tel").removeClass();
        $("#Uyari_tel").empty();
        $("#Uyari_tel").append("Lütfen boş bırakmayınız.!");
        $("#Uyari_tel").addClass("error");
  	} 
  	else{
		if (telKont($('.tel').val())) {
            $("#Uyari_tel").removeClass();
            $("#Uyari_tel").empty();
            $("#Uyari_tel").append("Başarılı.");
            $("#Uyari_tel").addClass("success");
      
        }
    	else{
            $("#Uyari_tel").removeClass();
            $("#Uyari_tel").empty();
            $("#Uyari_tel").append("Hatalı.");
            $("#Uyari_tel").addClass("error");
    }
  	}
    
  });

  $(".tc").blur(function(){
  	if ($(this).val()=="") {
		$("#Uyari_tc").removeClass();
        $("#Uyari_tc").empty();
        $("#Uyari_tc").append("Lütfen boş bırakmayınız.!");
        $("#Uyari_tc").addClass("error");
  	} 
  	else{
		if (tcKont_digit($('.tc').val())) {
            $("#Uyari_tc").removeClass();
            $("#Uyari_tc").empty();
        	if (tcKont_uzunluk($('.tc').val())) {
            	$("#Uyari_tc").removeClass();
            	$("#Uyari_tc").empty();
            	if ($('.tc').val().substr(0,1) != "0") {
                	$("#Uyari_tc").removeClass();
                	$("#Uyari_tc").empty();
                	var toplam=0;
                	for (var i = 0; i < $('.tc').val().length-1 ; i++) {
                    	toplam += $('.tc').val().substr(i,1);
                	}
                	var uznlk = $('.tc').val().length;
                	if (toplam.substr(toplam.length,1) == $('.tc').val().substr(uznlk,1)) {
                    	$("#Uyari_tc").removeClass();
                    	$("#Uyari_tc").empty();
                    	var tek = 0;
                    	var cift = 0;
                    	for (var i = 0; i < 9 ; i++) {
                        if (i%2==0) {
                            tek += parseInt($('.tc').val().substr(i,1));
                        } 
                        else{
                            cift += parseInt($('.tc').val().substr(i,1));
                        }   
                    }
                    var sonuc =0;
                    sonuc = ((tek*7)-cift);
                    sonuc +="";
                    	if (sonuc.substr(sonuc.length-1, 1) == $('.tc').val().substr(9,1)) {
                        	$("#Uyari_tc").removeClass();
                        	$("#Uyari_tc").empty();
                        	$("#Uyari_tc").append("Girmiş olduğun TC Doğru");
                        	$("#Uyari_tc").addClass("success");
                    	} 
                    	else{
                        	$("#Uyari_tc").removeClass();
                        	$("#Uyari_tc").empty();
                        	$("#Uyari_tc").append("Girmiş olduğun TC yalnış.(Tek-Çift)");
                        	$("#Uyari_tc").addClass("error");
                    	}
                	} 
                	else{
                	$("#Uyari_tc").removeClass();
                	$("#Uyari_tc").empty();
                	$("#Uyari_tc").append("Girmiş olduğun TC yalnış.(Son sayı)");
                	$("#Uyari_tc").addClass("error");
                    }
            	}
            	else{
                	$("#Uyari_tc").removeClass();
                	$("#Uyari_tc").empty();
                	$("#Uyari_tc").append("İlk karakter 0 olamaz.!");
                	$("#Uyari_tc").addClass("error");
            	}
        	}
        	else{
            	$("#Uyari_tc").removeClass();
            	$("#Uyari_tc").empty();
            	$("#Uyari_tc").append("Lütfen 11 karakter giriniz");
            	$("#Uyari_tc").addClass("error");
        	}
    	}
    	else{
        $("#Uyari_tc").removeClass();
        $("#Uyari_tc").empty();
        $("#Uyari_tc").append("Lütfen sayısal değer giriniz");
        $("#Uyari_tc").addClass("error");
    	}
  	}
    
  });

});

 

function isimKont(isim)
{
var kontrol = new RegExp(/[a-zA-Z]$/);
    return kontrol.test(isim);
}
function soyisimKont(soyisim)
{
var kontrol = new RegExp(/[a-zA-Z]$/);
    return kontrol.test(soyisim);
}

function urlKont(url)
{
var kontrol = new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/);
    return kontrol.test(url);
}

function mailKont(email)
{
var kontrol = new RegExp(/^[^0-9]\w+([.]\w+)*[@]\w+([.]\w+)*[.][a-zA-Z]{2,4}$/i);
return kontrol.test(email);
}

function telKont(tel)
{
var kontrol = new RegExp(/[0-9-()+]{3,20}/);
    return kontrol.test(tel);
}

function tcKont_digit(tc)
{
var kontrol = new RegExp(/\d/);
    return kontrol.test(tc);
}


function tcKont_uzunluk(tc)
{
var kontrol = new RegExp(/[0-9]{11}/);
    return kontrol.test(tc);
}
